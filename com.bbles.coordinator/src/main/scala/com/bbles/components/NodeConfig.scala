package com.bbles.components

import com.typesafe.config.{Config, ConfigFactory}

class NodeConfig(config: Config = ConfigFactory.load()) {
  val NODE_HOST_NAME_CONFIG = "node.host.name"
  val NODE_HOST_NAME_DEFAULT = "localhost"
  val NODE_HOST_PORT_CONFIG = "node.host.port"
  val NODE_HOST_PORT_DEFAULT = 9090

  def this(path: String) = this(ConfigFactory.load(path))

  if (config.hasPath("node.config.dir"))
    config.getConfig(config.getString("node.config.dir"))

  def port: Int = if (config.hasPath(NODE_HOST_PORT_CONFIG))
    config.getInt(NODE_HOST_PORT_CONFIG)
  else
    NODE_HOST_PORT_DEFAULT

  def host: String = if (config.hasPath(NODE_HOST_NAME_CONFIG))
    config.getString(NODE_HOST_NAME_CONFIG)
  else
    NODE_HOST_NAME_DEFAULT

  def id = config.getString("node.id")

}
