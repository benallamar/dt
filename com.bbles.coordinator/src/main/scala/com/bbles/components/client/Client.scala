package com.bbles.components.client

import com.twitter.io.Writer
import com.twitter.util.Closable

trait Client[S, T] {

  /**
    * Deliver a message to a node
    *
    * @param node       the node to connect with
    * @param serializer the serialized object to be communicate
    * @tparam V
    * @tparam U
    */
  def deliver[V >: S, U >: T](node: V, serializer: U)


  /**
    * Broadcast the given message to the list of given nodes
    * Either create a broadcast channel/group or do it iteratively
    *
    * @param nodes      the list of the node to be contacted
    * @param serializer the serialized information to be communicated
    * @tparam V
    * @tparam U
    */
  def broadcast[V >: S, U >: T](nodes: List[V], serializer: U): Boolean

  def deliver[U <: Writer with Closable](host: String, port: String, writable: U): Boolean = false
}
