package com.bbles.components


package object exceptions {

  class NoSuchMethodException extends NoSuchElementException

  class NoSuchRouteException extends NoSuchElementException

}
