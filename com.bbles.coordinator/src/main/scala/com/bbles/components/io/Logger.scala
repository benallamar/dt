package com.bbles.components.io

import com.sun.javafx.runtime.SystemProperties
import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory

import scala.Console._

trait Logger {

  //ConfigFactory.load()
  val log = LoggerFactory.getLogger(loggerName)

  def loggerName: String = "Distributed Tests"

  def debug(txt: String) = log.debug(s"$RESET$BLUE_B$txt$RESET")

  def info(txt: String) = log.info(s"$RESET$GREEN$txt$RESET")

  def error(txt: String) = log.error(s"$RESET$RED$txt$RESET")

  def warn(txt: String) = log.warn(s"$RESET$YELLOW$txt$RESET")

  val l = Option
}
