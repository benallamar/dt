package com.bbles.components.apis.controllers

import com.bbles.components.io.Logger
import com.bbles.components.io.serializers.{Serializer, StringSerializer}
import com.twitter.finagle.Service
import com.twitter.finagle.http._
import com.twitter.io.Reader
import com.twitter.util.Future
import com.bbles.nodes.Node

/*
A trait that simulate the basic RPC/HTTP/REST verbs
 */
abstract class BasicController extends Service[Request, Response] with Logger {
  private[this] var caller: Node = null

  /**
    * Implement the HTTP GET
    */
  def not_found[U <: Reader](req: Request, obj: U = Reader.Null): Future[Response] = toFutureResponse(Reader.Null, Status.NotFound, req.version)

  def get(req: Request): Future[Response] = not_found(req)

  def post(req: Request): Future[Response] = not_found(req)

  def render[U <: Reader](obj: U, status: Status = Status.Ok, version: Version = Version.Http10): Future[Response] = {
    toFutureResponse(obj, status, version)
  }

  def getCaller = caller

  def setCaller[U <: Node](_caller: U): Unit = {
    caller = _caller
  }

  def toFutureResponse[U <: Reader](obj: U, status: Status, version: Version): Future[Response] =
    Future.value(Response(version, status, obj))


  /**
    * Extract path from Request; look for a matching pattern; if found, dispatch the
    * Request to the registered service; otherwise create a NOT_FOUND response
    */
  def apply(request: Request): Future[Response] = {

    Either(this.getClass.getMethod(request.method.name.toLowerCase, Request.getClass)) match {
      case Right(Option[Method] ) => e.invoke(request),
      case Left(e) => not_found(request)
    }
  }

}
