package com.bbles

import javax.jnlp.FileContents

import com.bbles.components.NodeConfig
import com.bbles.components.apis.controllers.BasicController
import com.bbles.components.io.serializers.StringSerializer
import com.bbles.nodes.Node
import com.twitter.finagle.http.{Request, Response, Status}
import com.twitter.util.Future

object Main {
  def main(argvs: Array[String]): Unit = {
    /*
    Define one node server
     */
    val config: NodeConfig = new NodeConfig("node_1")
    val node: Node = new Node(config)

    class AuthController extends BasicController {
      def get(req: Request): Future[Response] = {
        render(new StringSerializer(getCaller.serialize))
      }
    }

    class FileController extends BasicController {
      /**
        * Get information about the file system (could be update to get more data)
        *
        * @param req Request to be handled
        * @return
        */
      def get(req: Request): Future[Response] = {
        render(null, Status.Ok, req.version)
      }
    }

    class HealthController extends BasicController {
      def get(req: Request): Future[Response] = {
        render(new StringSerializer("I'm ok"))
      }
    }
    node.setAPIHandler("file", new FileController)
    node.setAPIHandler("auth", new AuthController)
    node.setAPIHandler("health", new AuthController)
    /*
    start the node
     */
    node.start
  }
}