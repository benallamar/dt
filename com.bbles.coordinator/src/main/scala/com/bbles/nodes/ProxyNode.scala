package com.bbles.nodes

import com.bbles.components.NodeConfig
import com.bbles.components.client.Client
import com.bbles.components.io.serializers.Serializer


class ProxyNode(config: NodeConfig) extends Client[Node, Serializer] {
  /**
    * Deliver a message
    *
    * @param node    node to be contacted
    * @param message : Message to be delivered
    * @tparam Node
    * @tparam Serializer
    */
  override def deliver[Node, Serializer](node: Node, message: Serializer): Unit = {}

  override def broadcast[Node, Serializer](nodes: List[Node], message: Serializer): Boolean = true


  def healthCheck(): Boolean = false
}
