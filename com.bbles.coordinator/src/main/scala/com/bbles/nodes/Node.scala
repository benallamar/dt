package com.bbles.nodes

import com.bbles.components.NodeConfig
import com.bbles.components.apis.controllers.BasicController
import com.bbles.components.io.Logger
import com.bbles.components.io.serializers.StringSerializer
import com.bbles.components.server.Server
import com.twitter.finagle.http.{Request, Response, Status}
import com.twitter.util.Future
import org.json4s.JsonDSL._
import org.json4s.native.JsonMethods._
import scala.collection._

class Node(
            host: String,
            port: Int,
            private[Node] val _id: String,
            private[Node] var _parent: Node,
            private val children: mutable.HashSet[Node] = mutable.HashSet[Node]())
  extends Server(host, port) with Logger {
  /**
    * Set the child for the parent
    */
  override def loggerName = s"DataNode: $id"

  if (_parent != null)
    _parent.child(this)

  def this(config: NodeConfig) = this(config.host, config.port, config.id, null, mutable.HashSet[Node]())

  def this(id: String) = this("", 9000, id, null, mutable.HashSet[Node]())

  def this(id: String, parent: Node, children: mutable.MutableList[Node])
  = this("localhost", 9000, id, parent, mutable.HashSet[Node]())

  def id = _id

  def child(_child: Node): Unit = {
    info(s"Add children: ${_child}")
    children.add(_child)
    _child._parent = this
  }

  def parent_=(parent: Node) = {
    info(s"Set parent: ${parent}")
    _parent = parent
  }

  def isChild(_child: Node): Boolean = {
    children.contains(_child)
  }

  def parent = _parent

  def getchildren = children

  override def toString: String = _id

  /**
    * Serialize the node object
    *
    * @return
    */
  def toJobject = ("id" -> id) ~ ("name" -> host)

  def serialize = compact(render(toJobject))

  /**
    * Send a signal of live to all nodes (to update it's list)
    *
    * @return
    */
  def checkHealth(node: Node, timeout: Int): Boolean = ???


  /**
    * Update the list of the nodes
    */

  def updateChildList: Unit = ???


  /**
    * Check if it's an orphan node
    * the method check if the parent node is alive.
    * The parent node may leave the cluster for many reason(hardware/software failure).
    * Meanwhile, the lost of contact with the parent could be due to some network issues (slow connection).
    * Either we should consider this later as a failure or not, depends on the value we have set to
    * the connection timeout, so please be carefull.
    */

  def isOrphan: Boolean = true
}

object Node {

  def apply(id: String, parent: Node, children: mutable.MutableList[Node]) = new Node(id, parent, children)

  def apply(id: String) = new Node(id)

  def apply(config: NodeConfig) = new Node(config)

  def main(argvs: Array[String]): Unit = {
    val nodeConfig = new NodeConfig()
    val node = Node(nodeConfig)
    class Controller extends BasicController {
      override def get(req: Request): Future[Response] = {
        render(new StringSerializer("Element of the list"), Status.Ok)
      }
    }
    node.setAPIHandler("/hello-world", new Controller)
    node.start
  }
}