package components.api_handlers.controllers

import com.bbles.components.io.Logger
import com.bbles.components.io.serializers.Serializer
import com.twitter.finagle.Service
import com.twitter.finagle.http.{Method, Request, Response, Status, Version}
import com.twitter.util.Future

/*
A trait that simulate the basic RPC/HTTP/REST verbs
 */
abstract class BasicController extends Service[Request, Response] with Logger {
  /**
    * Implement the HTTP GET
    */

  def not_found(req: Request): Future[Response] = Future.value(Response(req.version, Status.NotFound))

  def get(req: Request): Future[Response] = not_found(req)

  def render[U <: Serializer](obj: U, status: Status): Future[Response] = {
    Future.value(
      Response(
        Version.Http10,
        status,
        obj)
    )
  }

  /**
    * This method calls other methods to response to a client request
    */
  def handle(req: Request): Future[Response] = {
    info(s"[${req.remoteHost}][${req.method.name}] ${req.path}")
    req.method match {
      case Method.Get => get(req)
      case _ => Future.value(Response(req.version, Status.NotFound)) // In case the handler doesn't exist
    }
  }

  def apply(req: Request): Future[Response] = handle(req)
}
