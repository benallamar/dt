package components.api_handlers

import com.bbles.components.io.Logger
import com.twitter.finagle.Service
import com.twitter.finagle.http.{HttpMuxer, Request, Response, Route}

class APIRegister(_routes: Seq[Route]) extends Logger {
  var httpMuxer: HttpMuxer = new HttpMuxer(_routes)

  def this() = this(Seq.empty[Route])

  override def loggerName = "API Register"

  def routers = httpMuxer

  /**
    * Add an end-point api and the underlying controller
    *
    * @param pattern    Support the endpoint defined by pattern
    * @param controller The controller handling the route
    */
  def register(pattern: String, controller: Service[Request, Response]) = {
    info(s"Register patter: $pattern, with handler: $controller")
    httpMuxer = httpMuxer.withHandler(pattern, controller)
  }

}

object APIRegister {
  def apply() = new APIRegister()
}