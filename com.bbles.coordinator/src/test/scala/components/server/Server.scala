package components.server

import com.bbles.components.apis.APIRegister
import com.twitter.finagle.http.{Request, Response}
import com.twitter.finagle.{Http, Service}
import com.twitter.util.Await

class Server(host: String, port: Int, var aPIRegister: APIRegister) {


  def this(port: Int) = this("", port, APIRegister());

  def this(host: String, port: Int) = this(host, port, APIRegister());

  /**
    * @return it return the api register responsible for answering requests
    */
  def serviceDefiner = Http.serve(s"$host:$port", aPIRegister.routers)

  /**
    * Register API for the server to know how to handle incoming request.
    * See [[APIRegister.register]]
    *
    * @param pattern    endpoint name (i.e: '/auth' for authentification)
    * @param controller APIRegister responsble for the behavior for the given action
    */
  def setAPIHandler(pattern: String, controller: Service[Request, Response]) {
    aPIRegister += (pattern, controller)
  }

  /**
    * Start the server.
    *
    * @return None
    */
  def start = Await.ready(serviceDefiner)

}


object Server {
  def apply(host: String, port: Int, aPIRegister: APIRegister) = new Server(host, port, aPIRegister)

  def apply(aPIRegister: APIRegister) = new Server("", 8080, aPIRegister)
}
