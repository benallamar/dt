package com.bbles.components

import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.native.JsonMethods._
import java.io.{File, FileInputStream, IOException}

import com.google.common.net.MediaType
import components.io.Logger
import org.mockserver.client.server.MockServerClient
import org.mockserver.integration.ClientAndServer
import org.mockserver.model.{HttpRequest, HttpResponse}

/**
  * Mock server for integration purpose
  */

class MockServerTest(port: Int) extends ClientAndServer(port) with Logger {
  def this() = this(10008)

  private class JsonFile(file: String) extends File(file)

  def register[U <: File](path: String, file: U) = {
    var is: FileInputStream = new FileInputStream(file)
    try {
      var jsonParser = render(parse(new StreamInput(is)))
      when(
        HttpRequest
          .request
          .withMethod("GET")
          .withPath(normalizePath(path))
      ).respond(
        HttpResponse
          .response
          .withBody(compact(jsonParser), MediaType.JSON_UTF_8)
      )
    } catch {
      case e: Exception => println(e.getMessage)
    } finally {
      is.close()
    }
    this
  }

  /**
    * Normalize the path
    *
    * @param path the path that should be normalized
    * @return normalized path
    */
  def normalizePath(path: String): String = {
    if (path.startsWith("/"))
      if (path.endsWith("/"))
        return path.substring(0, path.length - 2)
      else
        return path
    else {
      if (path.endsWith("/"))
        return "/" + path.substring(0, path.length - 2)
      else
        return "/" + path
    }

  }
}

object MockServerTest extends App {
  val mockerServer = new MockServerTest()
  mockerServer.register("/fuck-you", new File("src/test/resources/data/fuck-you.json"))
}
