package components.io.serializers

import com.twitter.io.{Reader, Writer}
import com.twitter.util.Closable

abstract class Serializer extends Reader with Writer with Closable

