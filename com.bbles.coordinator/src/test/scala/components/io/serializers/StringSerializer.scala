package components.io.serializers

import com.bbles.components.io.Logger
import com.twitter.io.Buf
import com.twitter.io.Buf.ByteArray
import com.twitter.util.{Future, Time}

class StringSerializer(var obj: String) extends Serializer with Logger {
  var currentIndex = 0

  override def loggerName = "StringSerializer"

  override def read(n: Int): Future[Option[Buf]] = {
    debug(s"reading the input $n")
    if (currentIndex < n) {
      currentIndex = n
      Future.value(
        Option[Buf](new ByteArray(obj.getBytes, 0, Math.min(n, length)))
      )
    } else {
      Future.None
    }
  }

  override def close(deadline: Time): Future[Unit] = {
    Future.value(
      {
        throw new Exception("Thank you")
      }
    )
  }

  override def write(buf: Buf): Future[Unit] = Future.value({})

  override def fail(cause: Throwable): Unit = {

  }

  override def discard(): Unit = {}

  def length(): Int = obj.getBytes.length
}
