package bbles

package object components {

  class NoSuchMethodException extends NoSuchElementException

  class NoSuchRouteException extends NoSuchElementException

}
