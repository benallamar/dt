package components.client

import com.twitter.io.Writer
import com.twitter.util.Closable

trait Client[+S, +T] {

  def deliver[V >: S, U >: T](node: V, serializer: U)

  def broadcast[V >: S, U >: T](nodes: List[V], serializer: U)

  def deliver[U <: Writer with Closable](host: String, port: String, writable: U): Boolean = ???
}
