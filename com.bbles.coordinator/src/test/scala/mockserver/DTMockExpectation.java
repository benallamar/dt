package mockserver;


import com.google.common.net.MediaType;
import components.io.Logger;
import org.mockserver.client.server.MockServerClient;
import org.mockserver.initialize.ExpectationInitializer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;

import java.io.FileInputStream;

public class DTMockExpectation implements ExpectationInitializer {
    public void initializeExpectations(MockServerClient mockServerClient) {

        mockServerClient
                .when(
                        HttpRequest.request("fuck-you")
                )
                .respond(
                        HttpResponse
                                .response()
                                .withBody("{\"merci\":thank you}", MediaType.JSON_UTF_8)
                );
    }
}
