package nodes

import com.bbles.components.NodeConfig
import com.bbles.components.io.serializers.Serializer
import com.bbles.nodes.{Node, ProxyNode}
import org.scalatest.FlatSpec

class ProxyNodeTest extends FlatSpec {
  val testConfig = new NodeConfig()
  val proxyNode = new ProxyNode(testConfig)
  val nodes = List[Node]()
  "ProxNode" should "Act as a reporter" in {
    it should "report the right value" in {
      // assert(proxyNode.broadcast(nodes))
    }

    it should "send a health check to the admin server" in {
      //  assert(proxyNode.healthCheck())
    }

    it should "deliver a message to given node" in {
      // assert(proxyNode.deliver(nodes.head))
    }
  }
}
