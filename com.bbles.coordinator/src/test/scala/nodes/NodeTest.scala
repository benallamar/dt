package com.bbles.nodes

import com.bbles.components.MockServerTest
import com.bbles.nodes.Node
import org.scalatest.FlatSpec

import scala.collection.mutable

class NodeTest extends FlatSpec {
  val mockserver = new MockServerTest
  private[this] val parent: Node = Node("parent", null, mutable.MutableList[Node]())
  private[this] val child: Node = Node("child", parent, mutable.MutableList[Node]())
  private[this] val dataNodes: mutable.MutableList[Node] = mutable.MutableList[Node](child, parent);


  "Node" should "behave safely and correctly" in {
    //mockserver.register("fuck-you")
    it should "id shoud be equal to the hash object" in {
      assert(dataNodes.find(f => f.id == child.id).size == 1)
    }

    it should "set the parent correctly" in {
      assert(child.parent == parent)
    }

    it should "set the children correclty" in {
      assert(parent.getchildren.contains(child))
    }

    it should "be able to communicate easily with it's parent" in {
      assert(child.heartbat())
    }
  }
}
