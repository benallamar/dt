package com.bbles

import com.bbles.components.Node
import com.twitter.io.Buf

object Main {
  def main(argvs: Array[String]): Unit = {
    val node: Node = Node("nodeOne")
    node.registerController(BasicController)
    node.start
  }
}