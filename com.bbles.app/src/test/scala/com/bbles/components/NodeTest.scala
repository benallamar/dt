package com.bbles.components

import org.scalatest.FlatSpec

import scala.collection.mutable

class NodeTest extends FlatSpec {
  private[this] val parent: Node = Node("parent", null, mutable.MutableList[Node]())
  private[this] val child: Node = Node("child", parent, mutable.MutableList[Node]())
  private[this] val nodes: mutable.MutableList[Node] = mutable.MutableList[Node](child, parent);


  "Node" should "behave safely and correctly" in {
    it should "id shoud be equal to the hash object" in {
      assert(nodes.find(_.getId == child.getId).size == 1)
    }

    it should "set the parent correctly" in {
      assert(child.getParent == parent)
    }

    it should "set the children correclty" in {
      assert(parent.getChildren.contains(child))
    }
  }
}
